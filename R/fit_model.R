
FitModel = function(forecast.method = NULL,
                    fitdata         = NULL,
                    fit             = NULL,
                    roll.opts       = NULL,
                    model.opts      = NULL) {
    
    # dat is the data used to fit the model and is a multivariate dataset with y as the variable being forecast
    y = ts(fitdata$y, frequency = roll.opts$freq.ts)
    #-----------------------------------------------------------------------------------
    if (forecast.method == "ETS") {
        fit = fit.ets(y, fit, model.opts)
    #-----------------------------------------------------------------------------------
    } else if (forecast.method == "ES") {
        fit = fit.es(y, fit, model.opts)
    #-----------------------------------------------------------------------------------
    } else if (forecast.method == "ESX") {
        fit = fit.esx(y, fitdata, fit, model.opts)
    #-----------------------------------------------------------------------------------
    } else if (forecast.method == "ETS-SES") {
        fit = fit.arima(y, fit, model.opts)
    #-----------------------------------------------------------------------------------
    } else if (forecast.method == "ETS-HOLT") {
        fit = fit.arima(y, fit, model.opts)
    #-----------------------------------------------------------------------------------
    } else if (forecast.method == "ETS-HOLTWINTERS") {
        fit = fit.arima(y, fit, model.opts)
    #-----------------------------------------------------------------------------------
    } else if (forecast.method == "HW") {
        fit = fit.HoltWinters(y, fit, model.opts)
    #-----------------------------------------------------------------------------------
    } else if (forecast.method == "SES") {
        fit = fit.ses(y, fit, model.opts)
    #-----------------------------------------------------------------------------------
    } else if (forecast.method == "ARIMA") {
        fit = fit.arima(y, fit, model.opts)
    #-----------------------------------------------------------------------------------
    } else if (forecast.method == "REG-STEP") {
        fit = fit.regstep(fitdata, fit, model.opts)
    #-----------------------------------------------------------------------------------
    } else if (forecast.method == "STLF") {
        fit = stl(y, s.window = "periodic")
    #-----------------------------------------------------------------------------------
    } else if (forecast.method == "BATS") {
        fit = bats(y)
    #-----------------------------------------------------------------------------------
    } else if (forecast.method == "TBATS") {
        if (is.null(fit) | model.opts$reoptimise == TRUE) fit = tbats(y)
        #fit = fit.tbats(y, fit, model.opts)
    #-----------------------------------------------------------------------------------
    } else if (forecast.method == "ARIMAX") {
        # not yet implemented, will require the arimax function from the TTR package
    #-----------------------------------------------------------------------------------
    } else if (forecast.method == "REG-ARIMA") {
        fit = fit.regarima(y, fitdata, fit, model.opts)
    #-----------------------------------------------------------------------------------
    } else if (forecast.method == "NNET") {
        fit = auto.arima(y)
    } else if (forecast.method == "SVM") {
        fit = auto.arima(y)
    } 
    
    if (roll.opts$TRACE > 0.30) {
        print(fit)
    }
    
    return(fit)
}

fit.es = function(y, fit=NULL, model.opts) {
    if (model.opts$reoptimise == TRUE | is.null(fit) == TRUE) {
        fit = es(y, silent=TRUE)
    } #else if (model.opts$reestimate == TRUE) {
       # fit = es(y, model = fit$model, persistence = fit$persistence, )
    #}
    return(fit)
}

fit.esx = function(y, fitdata, fit, model.opts) {
    if (model.opts$reoptimise == TRUE | is.null(fit) == TRUE) {
        model.opts$inc.AR = FALSE
        model.opts$harmonics = FALSE
        fit.reg = fit.regstep(fitdata, model.opts=model.opts) #AutoStepAIC(fitdata, roll.opts = roll.opts, model.opts = model.opts)
        xvars = variable.names(fit.reg, full = T)
        xvars = xvars[xvars != "(Intercept)"] #remove the = "Intercept"
        xreg = fitdata[, .SD, .SDcols = xvars]
        esx.model = es(data = y, xreg = xreg, silent = TRUE) #allowdrift=FALSE, approximation = TRUE, seasonal = FALSE) #  allowdrift = FALSE,
        fit = list(model = esx.model$model,
                   xreg.names = xvars)
    } 
}

fit.HoltWinters = function(y, fit = NULL, model.opts) {
    
    if (model.opts$reoptimise == TRUE | is.null(fit) == TRUE | model.opts$reestimate == TRUE) {
        fit = HoltWinters(y)
    } else {
        alpha = fit$alpha
        beta = fit$beta
        gamma = fit$gamma
        if (gamma > 0) {
            seasonal = fit$seasonal
            fit = HoltWinters(y, alpha = alpha, beta = beta, gamma = gamma, seasonal = seasonal)
        } else {
            fit = HoltWinters(y, alpha = alpha, beta = beta)
        }
    }
    return(fit)
}    

fit.ets = function(y, fit=NULL, model.opts) {
    # MONTHLY/WEEKLY  DATA
    if (model.opts$reoptimise == TRUE | is.null(fit) == TRUE) {
        # fit the best model
        if (roll.opts$freq.ts == 52) fit = ets(y, model = "ZZN") else fit = ets(y) 
    } else if (model.opts$reestimate == TRUE) {
        # this will refit the previous ETS model (i.e.)
        ets.damped = if (length(fit$components == 4)) as.logical(fit$components[4]) else FALSE
        ets.model = paste(fit$components[1:3], collapse="")
        #ets.model = substr(ets.components,start = 5, stop = (nchar(ets.components)-1))
        fit = ets(y, model = ets.model, damped = ets.damped)
    } else {
        fit = ets(y, model = fit)
    }
    return(fit)
}

fit.ses = function() {
    
    if (model.opts$reoptimise == TRUE | is.null(fit) == TRUE | model.opts$reestimate == TRUE) {
        fit = ses(y)
    } else {
        alpha = fit$alpha
        beta = fit$beta
        gamma = fit$gamma
        if (gamma > 0) {
            seasonal = fit$seasonal
            fit = HoltWinters(y, alpha = alpha, beta = beta, gamma = gamma, seasonal = seasonal)
        } else {
            fit = HoltWinters(y, alpha = alpha, beta = beta)
        }
    }
    return(fit)
}
fit.arima = function(y, fit=NULL, model.opts){
    # determine which action to take with the model
    if (model.opts$reoptimise == TRUE | is.null(fit) == TRUE) {
        fit = auto.arima(y, approximation = TRUE)
    } else if (model.opts$reestimate == TRUE) {
        #fit = Arima(x = y, model = fit, method = model.opts$optim.method)
        arima.order = fit$arma
        fit = Arima(y, order = arima.order[c(1,6,2)], seasonal = arima.order[c(3,7,4)], method = model.opts$optim.method  ) 
    } else {
        fit = Arima(y, model = fit, method = model.opts$optim.method)
    }
    return(fit)
}

fit.regarima = function(y, fitdata, fit=NULL, model.opts) {
    if (model.opts$reoptimise == TRUE | is.null(fit) == TRUE) {
        model.opts$inc.AR = FALSE
        model.opts$harmonics = TRUE
        fit.reg = fit.regstep(fitdata, model.opts=model.opts) #AutoStepAIC(fitdata, roll.opts = roll.opts, model.opts = model.opts)
        xvars = variable.names(fit.reg, full = T)
        xvars = xvars[xvars != "(Intercept)"] #remove the = "Intercept"
        xreg = fitdata[, .SD, .SDcols = xvars]
        regarima.model = auto.arima(x = y, xreg = xreg, allowdrift=FALSE, approximation = TRUE, seasonal = FALSE) #  allowdrift = FALSE,
        fit = list(model = regarima.model,
                   xvars = xvars)
    } else if (model.opts$reestimate == TRUE) { 
        xvars = fit$xvars
        xreg = as.matrix(fitdata[, .SD, .SDcols = xvars])
        arima.order = fit$model$arma
        regarima.model = tryCatch({
            Arima(y, order = arima.order[c(1,6,2)], include.drift=FALSE, xreg = xreg, method = model.opts$optim.method)  
        #}, warning = function(w) {
        #    message(paste("Orignal warning:", w))
        #    return(Arima(y, order = arima.order[c(1,6,2)], seasonal = arima.order[c(3,7,4)], xreg = xreg, method = model.opts$optim.method))
        }, error = function(e) {
            message(paste("Orignal error:", e))
            model.opts$optim.method = "ML"
            return(Arima(y, order = arima.order[c(1,6,2)], include.drift=FALSE, seasonal = arima.order[c(3,7,4)], xreg = xreg, method = model.opts$optim.method))
        })
        fit = list(model = regarima.model,
                   xvars = xvars)
    }
    return(fit)
}

fit.regstep = function(fitdata, fit=NULL, model.opts) {
    # FIT a new model, reestimate the parameters or use the original model
    if (model.opts$reoptimise == TRUE | is.null(fit) == TRUE) fit = AutoStepAIC(fitdata, roll.opts = roll.opts, model.opts=model.opts)
    if (model.opts$reestimate == TRUE) fit = lm(formula = Reduce(paste, deparse(formula(fit))), data = fitdata)
    return(fit)
}

fit.tbats = function(y, fit=NULL, model.opts) {
    if (is.null(fit) | model.opts$reoptimise == TRUE)  {
        fit = tbats(y) 
    } else {
        fit = tbats(y, model = fit)
    }
    return(fit)
}

fit.test = function() {
    mvts = PrepareRegDat(mvts1, roll.opts,model.opts)
    y = ts(mvts$y, frequency = roll.opts$freq.ts)
    x = FitModel(forecast.method = "ETS",fitdata = mvts,fit = NULL,roll.opts = roll.opts, model.opts = model.opts)
    accuracy(x)
    accuracy(HoltWinters(y))
    accuracy(holt(mvts$y))
    accuracy(ses(mvts$y))
    accuracy(h)
}