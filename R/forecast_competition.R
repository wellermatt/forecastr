# will accept a single item as either mvts or uvts
# will run a rolling origin forecasting run for each method

# will calculate the errors for each set of data,

# need to add in the following: fields in forecast or add them at a later point (requires access to the original ts):
# fc.naive/snaive, MAE.naive/snaive, MASE

# need to check the format, order and columns in the output from RollingForecast
ForecastingCompetition = function(mvts             = NULL,
                                  forecast.methods = NULL,
                                  model.opts       = NULL,
                                  roll.opts        = NULL) {

    #roll.opts$periodicity = unique(mvts$periodicity)
    roll.opts$freq.ts = if (roll.opts$periodicity == "W") 52 else 12
    roll.opts$h.max = if (roll.opts$periodicity == "W") 13 else 3

    mvts = PrepareRegDat(spi        = mvts,
                         roll.opts  = roll.opts,
                         model.opts = model.opts)


    if (roll.opts$best.fit == TRUE && length(forecast.methods) > 1) {
        optimal.method = ForecastingCompetitionInSample(mvts, forecast.methods = forecast.methods ,roll.opts, model.opts)$method.optimal
    } else {
        optimal.method = "NULL"
    }
    print(forecast.methods)
    forecasts = foreach(forecast.method = forecast.methods, .combine="dtcomb") %do% {
        print(paste(roll.opts$fc.item, forecast.method, sep=" --> "))
        fc = ForecastRoll(mvts = mvts, forecast.method = forecast.method,
                          model.opts = model.opts, roll.opts = roll.opts)
        
        if (roll.opts$best.fit == TRUE && optimal.method == forecast.method) {
            print(paste("BEST-FIT = ", forecast.method))
            fc.opt = copy(fc)
            fc.opt[, method:="BEST-FIT"]
            fc = rbindlist(l=list(fc, fc.opt))
        }
        fc
    }
    return(forecasts)
}

ForecastingCompetitionInSample = function(mvts             = NULL,
                                          forecast.methods = NULL,
                                          roll.opts        = NULL,
                                          model.opts       = NULL) {

    roll.times = GetRollPeriods(mvts, roll.opts)
    fitdata = mvts[period %in% roll.times$fit.periods]

    #====== IN SAMPLE ACCURACY FOR EACH FORECAST METHOD, RETURNING HE MAE FOR EACH METHOD
    in.sample.accuracy = foreach(forecast.method = forecast.methods, .combine="dtcomb") %do% {
        fitted.model = FitModel(forecast.method = forecast.method,
                                fit             = NULL,
                                fitdata         = fitdata,
                                roll.opts       = roll.opts,
                                model.opts      = model.opts)

        data.table(fc.item         = fitdata$fc.item[1],
                   forecast.method = forecast.method,
                   MAE             = InSampleMAE(fitted.model, forecast.method, roll.opts, fitdata$y))
    }

    return(list(method.optimal     = head(in.sample.accuracy[order(MAE)],1)$forecast.method,
                in.sample.accuracy = in.sample.accuracy))
}

InSampleMAE = function(fit, forecast.method, roll.opts, y=NULL) {

    if (forecast.method == "ETS") {
        tsp.start = tsp(fitted(fit))[1]
        tsp.end = tsp(fitted(fit))[2]
        dimnames(fitted(fit))[[2]][1]
        fc = if (class(fit) == "ets") as.numeric(fit$fitted) else fit$fitted[,1]
        act = window(x = fit$x,start = tsp.start, end = tsp.end)
        acc = accuracy(f = fc, x = act)
        MAE = acc[1, which(dimnames(acc)[[2]]=="MAE")]
    } else if (forecast.method %in% c("ARIMA", "REG-STEP")) {
        MAE = mean(abs(fit$residuals))
    } else if (forecast.method == ("REG-ARIMA")) {
        MAE = mean(abs(fit$model$residuals))
    } else if (forecast.method == "NAIVE") {
        MAE = mean(abs(diff(y)))
    } else if (forecast.method == "SNAIVE") {
        MAE = mean(abs(diff(y, lag = roll.opts$freq.ts)))
    } else if (forecast.method == "ES") {
        MAE = mean(abs(fit$actuals-fit$fitted))
    } else if (forecast.method == "HW") {
        MAE = mean(abs(fit$fitted-fit$x))
    } else if (forecast.method == "TBATS") {
        MAE = accuracy(fit)[3]
    }
    return(MAE)
}
