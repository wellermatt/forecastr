

StartForecastingCluster = function(cores = 1, 
                                   cluster.type = "FORK",
                                   outfile = "", 
                                   export.vars = "", 
                                   export.packages = c("doParallel", "forecastR", "foreach", 
                                                       "iterators", "data.table", "forecast", "reshape2","MASS")) {
    
    if (cores > 1) {
        
        cl <- makeCluster(cores, type = cluster.type, outfile = outfile)
        registerDoParallel(cl)
        
        print(paste("**** ", getDoParName(), "Cluster started with", getDoParWorkers(), "cores"))
        print("**** EXPORTED VARIABLES, FUNCTIONS AND PACKAGES ****")
        print(paste("VARIABLES:", export.vars))
        print(paste("PACKAGES:", export.packages))
        return(cl)
    }
}

