TestRollSingleItem = function(dat.in, h=1, forecast.methods) {
    roll.opts = DefaultRollOpts()
    model.opts = DefaultModelOpts()
    model.opts$reestimate = TRUE
    roll.opts$TRACE = 1
    roll.opts$cores = 8
    roll.opts$h.max = h
    roll.opts$optimal.method = FALSE
    run.time = system.time({fc = Roll(dt = dat.in, forecast.methods = forecast.methods, model.opts = model.opts, roll.opts = roll.opts)})
    print(run.time)
    fc
}
#library(forecastR)
data(mvts1,mvts20,mvts1m,mvts20m)
forecast.methods = c("REG-ARIMA","ETS", "REG-STEP", "ARIMA", "SNAIVE", "NAIVE")#, "ETS"))
output = TestRollSingleItem(mvts20, h = 13, forecast.methods = forecast.methods)
err = CalculateErrors(output)
err2 = SummariseErrors(err, groupby = 1)
err.melt = MeltErrors(err2, relative.only = TRUE)
err.melt[method=="REG-ARIMA" & variable == "gmrae.sn"]
err.melt[fc.item=="00-01-16000-66590"]

ggplot(err.melt, aes(x=method, y=value)) + geom_boxplot() + facet_wrap(~variable,scales = "free")


dcast(err2, fc.item~method, value.var = "gmrae.n", fun.aggregate = median)


input = mvts20[, list(fc.item, period, units.adj)]
input[, period:=period+52]
setkeyv(input,c("fc.item", "period"))
setkeyv(output,c("fc.item", "t"))
input[output]




######
library(forecastR)
data(mvts20, mvts1)
roll.opts = DefaultRollOpts( )
model.opts = DefaultModelOpts( )
roll.opts$h.max = 13
roll.opts$cores = 10
roll.opts$TRACE = 0

dt = mvts20
system.time({ e1 = Roll(dt = dt, forecast.methods = "REG-STEP", model.opts = model.opts, roll.opts=roll.opts) })

model.opts$reestimate = TRUE; model.opts$reoptimise = FALSE
system.time({ e2 = Roll(dt = dt, forecast.methods = "REG-STEP", model.opts = model.opts, roll.opts=roll.opts)[, method := "REG-STEP-REEST"] })

model.opts$reestimate = FALSE; model.opts$reoptimise = TRUE
system.time({ e3 = Roll(dt = dt, forecast.methods = "REG-STEP", model.opts = model.opts, roll.opts=roll.opts)[, method:="REG-STEP-REOPT"] })


fc = rbindlist(list(e1,e2,e3))
err= CalculateErrors(fc)
err.summary = SummariseErrors(err)
err.summary
dcast(err.summary,fc.item~method,fun.aggregate = mean,value.var = "smape")
