library(doParallel); library(parallel)
if (!require(smooth)) install_github("config-i1/smooth")

# build some test data - 10 distinct data.frame objects, each with a column as the identifier and one with the timeseries 
mydata = lapply(1:10,function(x) data.frame(series = x, y=rnorm(100)))

# test it in singular mode
myts = ts(mydata[[1]]$y)
es(myts, h=10)$forecast

# test all series with lapply rather than mclapply
lapply(mydata, FUN= function(x) {
    myts = ts(x$y)
    es(myts, h=10)$forecast
})

test.es.parallel = function(mydata, cores = 4, parallel.type = NULL, outfile = "") {
    
    # set up the parallel environment
    if (parallel.type == "SOCK") {
        cl = makePSOCKcluster(4, outfile=outfile)
    } else if (parallel.type == "FORK") {
        cl = makeForkCluster(4, outfile=outfile)
    } else if (parallel.type == "SEQ") {
    } else {
        cl = makeCluster(4, outfile=outfile)
    }
    
    if (parallel.type == "SEQ") {
        registerDoSEQ()
    } else {
        registerDoParallel(cl)
    }
    
    # test the es function in parallel
    out.data = mclapply(mydata, FUN= function(x) {
        myts = ts(x$y)
        es(myts, h=10, silent=TRUE)$forecast
    })
    stopCluster(cl)
    return(out.data)
}

test.es.parallel(mydata, 4, "", "")
test.es.parallel(mydata, 4, "SEQ", "")
test.es.parallel(mydata, 4, "FORK", "")
test.es.parallel(mydata, 4, "SOCK", "")


# test for ets function
if (!require(forecast)) install.packages("forecast")
results = mclapply(mydata, FUN = function(x) {
    myts = ts(x$y)
    fit = ets(myts)
    forecast(fit, h=10)$mean
})







foreach(x = list(mvts1),.packages="forecastR") %do%
{ 
    #z = PrepareRegDat(x,DefaultRollOpts(),DefaultModelOpts())
    #fit = FitModel("ETS", z, NULL, DefaultRollOpts(),DefaultModelOpts())
    roll.opts = DefaultRollOpts() 
    roll.opts = within(roll.opts, {
        TRACE = 1
        optimal.method = FALSE
        cores = 4
    })
    
    model.opts = DefaultModelOpts()
    model.opts = within(model.opts, {
        reoptimise = FALSE
        reestimate = TRUE 
        optim.method = "CSS"
    })
    ForecastMain(mvts20,"ETS",model.opts,roll.opts)
}

