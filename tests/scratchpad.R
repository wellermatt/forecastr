#library(forecast)
library(forecastR)
# library(data.table)
# library(doParallel)
# library(iterators)
# library(foreach)

data(mvts20, mvts1,
     GenericCalendarVariables.W, 
     GenericCalendarVariables.445)
# 
roll.opts = SetRollOpts()
model.opts = SetModelOpts()

results = Roll(dt = mvts1,methods = c("NAIVE","SNAIVE", "REG-STEP", "ARIMAX", "ETS"),model.opts,roll.opts)
rm(list=ls())

library(forecastR)
data(GenericCalendarVariables.445,GenericCalendarVariables.W, calendar.445, calendar.weekly)

load.data=FALSE
if (load.data == TRUE) {
    dat.w = readRDS("~/data/iri/categories/test/test_L3_W_all.rds")
    dat.m = readRDS("~/data/iri/categories/test/test_L3_M_all.rds")
    top.item = dat.w[,list(revenue=sum(revenue)), by="fc.item"][order(-revenue)]$fc.item[1]
    top.item = as.character(droplevels(top.item))
    
    dat.w.1 = dat.w[fc.item=="00-01-18200-00016"]
    dat.m.1 = dat.m[fc.item=="00-01-18200-00016"]    
}

# set options for forecasting
roll.opts = DefaultRollOpts() ; roll.opts$TRACE=TRUE roll.opts$cores = 4
model.opts = DefaultModelOpts()
methods = c("SNAIVE","NAIVE")#, "ARIMAX")
methods = "REG-STEP"
fc.w = Roll(mvts20, methods = methods, model.opts = model.opts, roll.opts = roll.opts)



results = ForecastingCompetition(mvts1, methods = c("NAIVE","SNAIVE"),
                                 model.opts, roll.opts)

#RollingForecastMVTS(mvts = mvts1, model.opts = model.opts, roll.opts = roll.opts) 
results = foreach(this.fc.item = unique(mvts20$fc.item), .combine="dtcomb") %do%
{
    mvts = mvts20[fc.item == this.fc.item]
    x = ForecastingCompetition(mvts = mvts, model.opts = model.opts, roll.opts=roll.opts, 
                             methods = c("NAIVE","SNAIVE", "ETS", "ARIMAX", "REG-STEP"))
    print(x)
    x
}


saveRDS(results,file = "~/data/iri/output/results/resultsTop20.rds")
ggplot(data = results, aes(y=smdape, x=method)) + geom_boxplot() + theme_bw()+coord_flip()
ggplot(data = results, aes(y=smdape, x=method)) + geom_boxplot() + facet_wrap(~horizon.month)+ theme_bw()+coord_flip()
dcast(results,formula = method~horizon.month,fun.aggregate = median,value.var = "smdape")
dcast(results,formula = method~.,fun.aggregate = median,value.var = "smdape")

x=ForecastingCompetition(mvts = mvts1, model.opts = model.opts, roll.opts=roll.opts, 
                         methods = c("NAIVE","SNAIVE", "ETS", "ARIMAX", "REG-STEP"))

comp = x





x=Roll(dat.in=mvts1, model.opts = model.opts, roll.opts = roll.opts, method = "REG-STEP") ; print(x)


x=Roll(dat.in=mvts1, model.opts = model.opts, roll.opts = roll.opts, method = "REG-STEP") ; print(x)
x=Roll(dat.in=mvts1, model.opts = model.opts, roll.opts = roll.opts, method = "ARIMAX") ; print(x)
x=Roll(dat.in=mvts1, model.opts = model.opts, roll.opts = roll.opts, method = "ETS") ; print(x)
err=CalculateErrors(x,roll.opts=roll.opts)
err2=SummariseErrors(err)
err2

x=Roll(dat.in=mvts20, model.opts = model.opts, roll.opts = roll.opts, method = "REG-STEP")
dt=mvts1

str(dt)
names(dt)
class(dt$fc.item)
key(dt)
library(foreach)
library(iterators)
library(data.table)
fc = foreach(dt.sub = isplitDT(dt, levels(dt$fc.item)),.export = "data.table") %do%
{
    dt.sub$key
    # code to execute on each node
    #roll.opts$fc.item = dt.sub$key[1]
    #if (roll.opts$TRACE==TRUE) print(roll.opts$fc.item)
    #RollingForecastMVTS(mvts = dt.sub$value, model.opts = model.opts, roll.opts = roll.opts) 
}

#######
model.opts$inc.AR=FALSE
model.opts$harmonics = FALSE

fit = AutoStepAIC(dat, roll.opts = roll.opts, model.opts = model.opts)
accuracy(fit)
xvars = variable.names(fit,full = T)
xvars = xvars[xvars!="(Intercept)"] #= "Intercept"
xreg <- as.matrix(dat[, .SD, .SDcols = xvars])
x = ts(dat$y, freq=52)

fit = auto.arima(x = x, xreg = xreg,seasonal = FALSE)
forecast(fit,13)$mean
accuracy(f = fit)
fit

fit = HoltWinters(x,)
accuracy(f = fit)
fit

x=Roll(dat.in=mvts1, model.opts = model.opts, roll.opts = roll.opts, "REG-STEP")
# timings

system.time(x<-Roll(dat.in=mvts1, model.opts = model.opts, roll.opts = roll.opts, "REG-STEP"))
system.time(x<-Roll(dat.in=mvts1, model.opts = model.opts, roll.opts = roll.opts, "ARIMA"))
system.time(x<-Roll(dat.in=mvts1, model.opts = model.opts, roll.opts = roll.opts, "ETS"))
system.time(x<-Roll(dat.in=mvts1, model.opts = model.opts, roll.opts = roll.opts, "NAIVE"))
system.time(x<-Roll(dat.in=mvts1, model.opts = model.opts, roll.opts = roll.opts, "SNAIVE"))

# 
#
# x.err = CalculateErrors(x,roll.opts = roll.opts)
# print(err.summary = SummariseErrors(err = x.err))
# 
# mvts.test = mvts20[fc.item==sample(fc.item,1)]
# x = RollingForecastMVTS(mvts=mvts.test, model.opts = model.opts, roll.opts = roll.opts)
# x
# 
# 
# print(SummariseErrors(CalculateErrors(x, roll.opts)))
# x=RollingForecastUVTS(uvts=mvts1$units.adj, method = "ETS", model.opts, roll.opts = roll.opts)
# x=RollingForecastUVTS(uvts=mvts1$units.adj, method = "ARIMA", model.opts, roll.opts = roll.opts)
# x=RollingForecastUVTS(uvts=mvts1$units.adj, method = "TBATS", model.opts,roll.opts = roll.opts)

# 
# M1.monthly.industry <- subset(M1,12,"industry")
# 
# M3.other <- subset(M3,"other", "allother")
# M3.other
# 
# GenericCalendarVariables.W <<- GenericCalendarVariables(52)
# GenericCalendarVariables.445 <<- GenericCalendarVariables(12)
# 

# start.date=as.Date("2001-01-01")
# start.date+1
# start.date+7
# start.date+(7*calendar.445$elapsed_weeks)
# startdates=c(start.date,start.date+(7*calendar.445$elapsed_weeks))
# start.dates
# startdates
calendar.445$start_date=startdates

#mvts1=droplevels(mvts1)
#setkeyv(mvts1,"fc.item")
#devtools::use_data(mvts1,mvts20,overwrite = TRUE)#   GenericCalendarVariables.445,GenericCalendarVariables.W)
devtools::use_data(mvts1m,overwrite = TRUE)#   GenericCalendarVariables.445,GenericCalendarVariables.W)

data(mvts1)
items = unique(mvts1$fc.item)

data(GenericCalendarVariables.445,GenericCalendarVariables.W, calendar.445, calendar.weekly)
library(data.table)
dat.w = readRDS("~/data/iri/categories/test/test_L3_W_all.rds")[fc.item %in% items]
dat.m = readRDS("~/data/iri/categories/test/test_L3_M_all.rds")[fc.item %in% items]

mvts1m = dat.m
mvts20m
# 
# 
# testset=mvts1[period>208]
# trainset=mvts1[period<=208]
# install.packages("rpart.plot")
# 
# library(rpart); library(rpart.plot)
# fit = rpart(formula = units.adj~FEAT_A+DISP_MAJOR, data = trainset, method = "anova")
# rpart.plot(fit)
# 
# # gdboost
# data(mvts1)
# data(cale)
# 
# model.opts <- list(dv = "units.adj",
#                    intercept = TRUE, calendar = TRUE, harmonics = TRUE, trend = TRUE,
#                    promo.vars = 1, price.terms = "price_diff",
#                    inc.AR = FALSE, log.model = FALSE)
# roll.opts = list(method="REG-STEP", h.max = 2, freq.ts=52, periodicity="W")
# 
# RollingForecast(mvts=mvts1, method="REG-STEP", roll.opts = roll.opts, model.opts = model.opts)

# 

#fil = paste0("~/data/iri/categories/test/test_L3_W_all.rds")
#mvts = readRDS(fil)    
#devtools::use_data(mvts20)#,calendar.weekly, calendar.445)
#mvts[]
#fc.item1 = 
#    mvts[
#    order(-revenue),
#    list(revenue=sum(revenue)),
#    by=fc.item
#    ][j=as.character(fc.item)]
#mvts=mvts[fc.item %in% fc.item1[1:20]]
#mvts1=mvts[fc.item %in% fc.item1[1]]
